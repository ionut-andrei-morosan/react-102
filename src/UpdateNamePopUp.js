import React from "react";

class UpdateNamePopUp extends React.Component {
  constructor(props) {
    super(props);

    this.textInput = React.createRef();
    this.state = {
      name: "",
      showModal: true
    };
  }

  closeModal = () => {
    this.setState({showModal: false});
  }

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ name: this.textInput.current.value });
  };

  render() {
    return (
      <div className={`alert-component ${this.state.showModal ? '' : 'hidden'}`}>
        <button className="close" onClick={this.closeModal}>
        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-x-square-fill" viewBox="0 0 16 16">
          <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm3.354 4.646L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 1 1 .708-.708z"/>
        </svg>
        </button>
        <h2>
          Welcome, <span>{!!this.state.name ? this.state.name : "User"}!</span>
        </h2>
        <form className="form" onSubmit={this.handleSubmit}>
          <div class="field">
            <label for="name">Please update your name</label>
            <div class="control">
              <input
                type="text"
                ref={this.textInput}
                name="name"
              />
            </div>
          </div>
          <div class="field">
            <div class="control">
              <button class="button is-dark">Update</button>
            </div>
          </div>
        </form>

      </div>
    );
  }
}

export default UpdateNamePopUp;
