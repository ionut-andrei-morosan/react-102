import React from "react"

export default function Card() {

  return (
    <div className='card-component'>
      <img src="https://place-hold.it/200x200" alt="" />
      <h3>Lorem Ipsum</h3>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus iusto quaerat libero fugit voluptas! Sint tempora saepe cupiditate veniam quam iste repellendus dicta eius nisi, repudiandae maiores magnam quo earum!</p>
    </div>
  )
}