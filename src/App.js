import React from 'react';
import Header from './Header';
import Nav from './Nav';
import Card from './Card';
import Footer from './Footer';
import { ThemeContext } from './ThemeContext';
import { useContext } from "react";
import './App.css';
import Preferences from './Preferences';
import UpdateNamePopUp from './UpdateNamePopUp';
import CardsWrapper from './CardsWrapper';

export default function App() {
  const {darkTheme, ctaTheme} = useContext(ThemeContext);

  return (
    <div className={`App ${ctaTheme} ${darkTheme ? "dark-theme" : ""}`}>
      <Header>
        <Nav/>
      </Header>

      <Preferences/>

      <CardsWrapper>
        <Card />
        <Card />
        <Card />
      </CardsWrapper>

      <UpdateNamePopUp />
     
      <Footer />
    </div>
  );
}
