import React from "react"
import { ThemeContext } from './ThemeContext'
import { useContext } from "react";

export default function Header({ children }) {
  const {darkTheme, setDarkTheme} = useContext(ThemeContext);

  return (
    <header className='header'>
      <a href="/#">
        <img src="https://place-hold.it/120x60" alt="Logo" />
      </a>

      {children}
      

      <button onClick={() => setDarkTheme(prevDarkTheme => !prevDarkTheme)}>Theme</button>
    </header>
  )
}