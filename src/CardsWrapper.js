import React from "react";

export default function CardsWrapper({ children }) {
  return (
    <>
      <div className="row">
        {children}
      </div>
      <div className="button-wrapper">
        <button>Buy</button>
      </div>
    </>
  );
}