import React from "react";

export default function Nav() {
  return (
    <nav>
        <ul>
          <li>
            <a href="/#">Home</a>
          </li>
          <li>
            <a href="/#">Services</a>
          </li>
          <li>
            <a href="/#">Techs</a>
          </li>
          <li>
            <a href="/#">Contact</a>
          </li>
        </ul>
    </nav>
  );
}