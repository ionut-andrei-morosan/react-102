import React, { useState } from 'react';

export const ThemeContext = React.createContext();
// const ThemeUpdateContext = React.createContext();

//Custom Hooks - Start
// export function useTheme() {
//   return useContext(ThemeContext);
// }

// export function useThemeUpdate() {
//   return useContext(ThemeUpdateContext);
// }
//Custom Hooks - End

export function ThemeProvider({ children }) {
  const [darkTheme, setDarkTheme] = useState(true);
  const [ctaTheme, setCtaTheme] = useState('default-cta-theme');

  // function toggleTheme() {
  //   setDarkTheme(prevDarkTheme => !prevDarkTheme);
  // }

  // function toggleCtaTheme() {
  //   setCtaTheme(prevDarkTheme => !prevDarkTheme)
  // }

  return (
    <ThemeContext.Provider value={{
      darkTheme, setDarkTheme,
      ctaTheme, setCtaTheme,
    }}>
        {children}
      {/* <ThemeUpdateContext.Provider value={toggleTheme} > */}
      {/* </ThemeUpdateContext.Provider> */}
    </ThemeContext.Provider>

    // <ThemeContext.Provider value={{darkTheme, ctaTheme}}>
    //   <ThemeUpdateContext.Provider value={{toggleTheme, toggleCtaTheme}} >
    //     {children}
    //   </ThemeUpdateContext.Provider>
    // </ThemeContext.Provider>
  )
}