import React, { useContext } from "react"
import { ThemeContext } from "./ThemeContext";

export default function Preferences() {
  const {setCtaTheme} = useContext(ThemeContext);

  return (
    <div className='preferences-component'>
      <select onChange={(e) => setCtaTheme(e.target.value)}>
        <option value="cta-default-theme">Default CTA theme</option>
        <option value="cta-octopus-theme">Octopus CTA theme</option>
        <option value="cta-gold-theme">Gold CTA theme</option>
        <option value="cta-fire-theme">Fire CTA theme</option>
        <option value="cta-avocado-theme">Avocado CTA theme</option>
        <option value="cta-flamingo-theme">Flamingo CTA theme</option>
      </select>
    </div>
  )
}